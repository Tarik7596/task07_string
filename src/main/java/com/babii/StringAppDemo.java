package com.babii;

import com.babii.UI.MainMenu;
import com.babii.model.TextHolder;

public class StringAppDemo {
    public static void main(String[] args) {
        new MainMenu(new TextHolder()).showLocalizationMenu();
    }
}
