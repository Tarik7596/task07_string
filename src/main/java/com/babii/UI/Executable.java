package com.babii.UI;

@FunctionalInterface
public interface Executable {
    void execute();
}
