package com.babii.UI;

import com.babii.model.Publishable;
import com.babii.servicepack.TextProcessor;
import com.babii.view.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static com.babii.servicepack.I18nSettings.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import static com.babii.servicepack.I18nSettings.setLogMsg;
import static com.babii.servicepack.I18nSettings.setTxtMenuBundle;

public class MainMenu {
    public static final Logger mainLog = LogManager.getLogger(MainMenu.class);
    private Publishable dataSource;
    private Map<Integer, String> textMenuMap;
    private Map<Integer, Executable> exeMenuMap;
    private BufferedReader br;

    public MainMenu(Publishable ds) {
        dataSource = ds;
        br = new BufferedReader(new InputStreamReader(System.in));
        prepareExecMenu();
    }
    private void showPause() throws IOException {
        mainLog.info(getLogMsg().getString("pause"));
        br.readLine();
    }
    public void showLocalizationMenu() {
        String langChoice;
        mainLog.info("Select language en or uk: ");
        try {
            langChoice = br.readLine();
            if(langChoice.toLowerCase().matches("en|uk")) {
                if(langChoice.matches("en")) {
                    setTxtMenuBundle(ResourceBundle.getBundle("TextMenu", getLocaleUs()));
                    setLogMsg(ResourceBundle.getBundle("LogMessages", getLocaleUs()));
                } else {
                    setTxtMenuBundle(ResourceBundle.getBundle("TextMenu", getLocaleUkraine()));
                    setLogMsg(ResourceBundle.getBundle("LogMessages", getLocaleUkraine()));
                }
                prepareTxtMenu();
                showTxtMenu();
                int execChoice = Integer.parseInt(br.readLine());
                processChoice(execChoice);
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException ex) {
            mainLog.warn("Unsupported language");
            showLocalizationMenu();
        } catch (IOException e) {
            mainLog.error("Error while reading from console");
        }
    }
    private void processChoice(int execChoice) throws IOException{
        exeMenuMap
                .entrySet()
                .stream()
                .filter(e -> e.getKey() == execChoice)
                .forEach(e -> e.getValue().execute());
        showPause();
        showTxtMenu();
        processChoice(Integer.parseInt(br.readLine()));
    }
    private void prepareTxtMenu() {
        textMenuMap = new LinkedHashMap<>();
        textMenuMap = IntStream.range(0, 17)
                .boxed()
                .collect(Collectors.toMap(
                        Function.identity(),
                        v -> getTxtMenuBundle().getString(String.valueOf(v))
                ));
    }
    private void showTxtMenu() {
        textMenuMap
                .entrySet()
                .forEach(e -> mainLog.info(e.getValue() + "\n"));
        mainLog.info(getLogMsg().getString("inpChoice") + " ");
    }
    private void prepareExecMenu() {
        exeMenuMap = new HashMap<>();
        exeMenuMap.put(0, this::exitProgram);
        exeMenuMap.put(1, this::calcSameWordSentence);
        exeMenuMap.put(2, this::filterSentByWordCount);
        exeMenuMap.put(3, this::showFirstUniqueWord);
        exeMenuMap.put(4, this::selectFromQuestion);
        exeMenuMap.put(5, this::swapFirstWord);
        exeMenuMap.put(6, this::showShiftedABOrder);
        exeMenuMap.put(7, this::sortByVowel);
        exeMenuMap.put(8, this::sortByConsonant);
        exeMenuMap.put(9, this::sortByLetter);
        exeMenuMap.put(10, this::sortByWord);
        exeMenuMap.put(11, this::cutFromSentence);
        exeMenuMap.put(12, this::cutWordLength);
        exeMenuMap.put(13, this::sortByLetter);
        exeMenuMap.put(14, this::findPalindrome);
        exeMenuMap.put(15, this::cutLetterFromWord);
        exeMenuMap.put(16, this::replaceBySequence);
    }
    private void replaceBySequence() {
        mainLog.info(getLogMsg().getString("wordLength"));
        try {
            int wordLength = Integer.parseInt(br.readLine()) + 1;
            mainLog.info(getLogMsg().getString("rpl"));
            String rplcmnt = " " + br.readLine();
            Updatable monitor = new SentenceCutView(wordLength, rplcmnt);
            prepareObserver(monitor);
        } catch (IOException e) {
            mainLog.error("Input reading error");
        }
    }
    private void cutLetterFromWord() {
        Updatable monitor = new MappedWordView(TextProcessor::cutLetter);
        prepareObserver(monitor);
    }
    private void findPalindrome() {
        Updatable monitor = new PalindromeSentenceView();
        prepareObserver(monitor);
    }
    private void cutWordLength() {
        mainLog.info(getLogMsg().getString("wordLength"));
        try {
            int wordLength = Integer.parseInt(br.readLine());
            Updatable monitor = new SentenceCutView(wordLength, "");
            prepareObserver(monitor);
        } catch (IOException e) {
            mainLog.error("Input reading error");
        }
    }
    private void cutFromSentence() {
        mainLog.info(getLogMsg().getString("cutFrom"));
        try {
            String startMark = br.readLine().trim();
            Updatable monitor = new SentenceCutView(startMark);
            prepareObserver(monitor);
        } catch (IOException e) {
            mainLog.error("Input reading error");
        }
    }
    private void sortByWord() {
        mainLog.info(getLogMsg().getString("wordSort"));
        try {
            String word = br.readLine().trim();
            Updatable monitor = new SortByWordSentenceView(word);
            prepareObserver(monitor);
        } catch (IOException e) {
            mainLog.error("Input reading error");
        }
    }
    private void sortByLetter() {
        mainLog.info(getLogMsg().getString("letterSort"));
        try {
            String letter = br.readLine().trim();
            Updatable monitor = new SortedByLetterWordView(letter);
            prepareObserver(monitor);
        } catch (IOException e) {
            mainLog.error("Input reading error");
        }
    }
    private void sortByConsonant() {
        Updatable monitor = new SortedVowelWordView(TextProcessor.compareByFirstConsonant);
        prepareObserver(monitor);
    }
    private void sortByVowel() {
        Updatable monitor = new SortedWordView(TextProcessor.compareVowelPercentage);
        prepareObserver(monitor);
    }
    private void showShiftedABOrder() {
        Updatable monitor = new ShiftedWordView();
        prepareObserver(monitor);
    }
    private void swapFirstWord() {
        Updatable monitor = new SentenceMapStreamView(TextProcessor::swapFirstWord);
        prepareObserver(monitor);
    }
    private void selectFromQuestion() {
        mainLog.info(getLogMsg().getString("wordLength"));
        try {
            int wordLength = Integer.parseInt(br.readLine());
            Updatable monitor = new WordsFromQuestion(wordLength);
            prepareObserver(monitor);
        } catch (IOException e) {
            mainLog.error("Input reading error");
        }
    }
    private void showFirstUniqueWord() {
        Updatable monitor = new FirstUniqueWord();
        prepareObserver(monitor);
    }
    private void filterSentByWordCount() {
        Updatable monitor = new SentenceFilterStreamView(TextProcessor.sentenceByWordCount);
        prepareObserver(monitor);
    }
    private void calcSameWordSentence() {
        Updatable monitor = new CountSentenceView(TextProcessor::filterMultiWords);
        prepareObserver(monitor);
    }
    private void exitProgram() {
        System.exit(0);
    }
    private void prepareObserver(Updatable monitor) {
        dataSource.subscribeObserver(monitor);
        dataSource.notifyObservers();
        dataSource.removeObserver(monitor);
    }
}
