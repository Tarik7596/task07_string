package com.babii.model;

import com.babii.view.Updatable;
import java.util.function.Supplier;
import java.util.stream.Stream;

public interface Publishable {
    void subscribeObserver(Updatable obs);
    void removeObserver(Updatable obs);
    void notifyObservers();
    Supplier<Stream<String>> wordsStream();
    String getAsString();
    Supplier<Stream<String>> sentenceStream();
}
