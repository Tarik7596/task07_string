package com.babii.model;

import com.babii.view.Updatable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TextHolder implements Publishable {
    private static final Logger modLogger = LogManager.getLogger(TextHolder.class);
    private List<Updatable> obsList;
    private List<String> txtStringList;

    public TextHolder() {
        obsList = new ArrayList<>();
        txtStringList = new LinkedList<>();
        fillTxtStorage();
    }
    private void fillTxtStorage() {
        try(Stream<String> lineStream = Files.lines(Paths.get("textbook.txt"))) {
            txtStringList = lineStream.collect(Collectors.toList());
        } catch (IOException e) {
            modLogger.error("Error reading file");
        }
    }

    public void subscribeObserver(Updatable obs) {
        obsList.add(obs);
    }
    public void removeObserver(Updatable obs) {
        int obsIndex  = obsList.indexOf(obs);
        if(obsIndex >= 0) {
            obsList.remove(obsIndex);
        }
    }
    public void notifyObservers() {
        obsList.forEach(o -> o.update(this));
    }
    public Supplier<Stream<String>> wordsStream() {
        return () -> txtStringList
                .stream()
                .flatMap(s -> Arrays.stream(s.split("\\W+")));
    }
    public Supplier<Stream<String>> sentenceStream() {
        String concatText = getAsString();
        return () -> Arrays.stream(concatText.split("(?<=[\\w\"\\)\\]][.?!])\\s+"));
    }
    public String getAsString() {
        return txtStringList
                .stream()
                .filter(s -> !s.isEmpty())
                .collect(Collectors.joining());
    }
}
