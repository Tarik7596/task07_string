package com.babii.servicepack;

import java.util.Locale;
import java.util.ResourceBundle;

public class I18nSettings {
    private static Locale localeUs;
    private static Locale localeUkraine;
    private static ResourceBundle txtMenuBundle;
    private static ResourceBundle logMsg;
    static {
        localeUs = new Locale.Builder().setLanguage("en").setRegion("US").build();
        localeUkraine = new Locale.Builder().setLanguage("uk").setRegion("UA").build();
    }
    public static Locale getLocaleUs() {
        return localeUs;
    }
    public static Locale getLocaleUkraine() {
        return localeUkraine;
    }
    public static ResourceBundle getTxtMenuBundle() {
        return txtMenuBundle;
    }
    public static void setTxtMenuBundle(ResourceBundle txtMenuBundle) {
        I18nSettings.txtMenuBundle = txtMenuBundle;
    }
    public static ResourceBundle getLogMsg() {
        return logMsg;
    }
    public static void setLogMsg(ResourceBundle logMsg) {
        I18nSettings.logMsg = logMsg;
    }
}
