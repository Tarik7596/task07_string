package com.babii.servicepack;

import java.util.Arrays;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextProcessor {
    public static Comparator<String> sentenceByWordCount =
            Comparator.comparingInt(TextProcessor::countWords);
    public static Comparator<String> compareVowelPercentage =
            Comparator.comparingInt(TextProcessor::countVowelPercentage);
    public static Comparator<String> compareByFirstConsonant =
            Comparator.comparingInt(TextProcessor::getConsonantIndex);
    public static boolean filterMultiWords(String s) {
        Pattern doubleWord = Pattern.compile("\\b(\\w+)\\b.+?\\1\\b", Pattern.CASE_INSENSITIVE);
        Matcher sentence = doubleWord.matcher(s);
        return sentence.find();
    }
    public static String swapFirstWord(String sentence) {
        String[] words = sentence.split("\\W");
        String longWord = Arrays.stream(words)
                .max(Comparator.comparing(String::length))
                .get();
        Pattern vowelWord = Pattern.compile("\\b[aeuioy]\\w+?\\b", Pattern.CASE_INSENSITIVE);
        Matcher testSentence = vowelWord.matcher(sentence);
        if(testSentence.find()) {
            sentence = testSentence.replaceFirst(longWord);
        }
        return sentence + "\n";
    }
    private static int countVowelPercentage(String word) {
        int count = 0;
        int vowelPercent;
        Pattern vowelLetter = Pattern.compile("[eyuioa]", Pattern.CASE_INSENSITIVE);
        Matcher target = vowelLetter.matcher(word);
        while(target.find()) {
            count++;
        }
        vowelPercent = count * 100 / word.length();
        return vowelPercent;
    }
    private static int getConsonantIndex(String word) {
        word = word.trim();
        Pattern consonantLetter = Pattern.compile("([^eyuioa]\\w+)\\b");
        Matcher target = consonantLetter.matcher(word);
        target.find();
        return target.start();
    }
    public static boolean findPalindrome(String sentence) {
        Pattern polindrome = Pattern.compile("^((.)(?:(\1?)|.?)\2)$");
        Matcher target = polindrome.matcher(sentence);
        return target.find();
    }
    public static String cutLetter(String word) {
        String cutLetter = String.valueOf(word.charAt(0));
        Pattern patt = Pattern.compile(cutLetter);
        Matcher target = patt.matcher(word.trim());
        word = target.replaceAll("");
        return word;
    }
    private static int countWords(String sentence) {
        int count = 0;
        Pattern word = Pattern.compile("\\b\\w+\\b");
        Matcher target = word.matcher(sentence);
        while(target.find()) {
            count++;
        }
        return count;
    }
}
