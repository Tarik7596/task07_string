package com.babii.view;

import com.babii.model.Publishable;
import org.apache.logging.log4j.Logger;

import static com.babii.UI.MainMenu.mainLog;

abstract class AbsStreamView implements Updatable {
    Publishable textSource;
    Logger appLogger = mainLog;
    public void update(Publishable source) {
        textSource = source;
        show();
    }
    abstract void show();
}
