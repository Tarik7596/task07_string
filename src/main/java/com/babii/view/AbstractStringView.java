package com.babii.view;

import com.babii.model.Publishable;

abstract class AbstractStringView implements Updatable {
    private String sourceString;

    public void update(Publishable source) {
        sourceString = source.getAsString();
        show();
    }
    abstract void show();
}
