package com.babii.view;

import static com.babii.servicepack.I18nSettings.*;
import java.util.function.Predicate;

public class CountSentenceView extends AbsStreamView {
    private Predicate<String> filterRule;

    public CountSentenceView(Predicate<String> fr) {
        super();
        filterRule = fr;
    }
    void show() {
        long timesMatch = textSource.sentenceStream()
                .get()
                .filter(filterRule)
                .map(s -> s + "\n")
                .count();
        appLogger.info(String.format("%d %s%n", timesMatch,
                getLogMsg().getString("matchTimes")));
    }
}
