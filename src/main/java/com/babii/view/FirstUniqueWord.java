package com.babii.view;

import java.util.Arrays;

public class FirstUniqueWord extends AbsStreamView {
    void show() {
        String[] firstSentenceWords = textSource
                .sentenceStream()
                .get()
                .limit(1)
                .flatMap(s -> Arrays.stream(s.split("\\W+")))
                .toArray(String[]::new);
        for(String s : firstSentenceWords) {
            if(textSource.getAsString().lastIndexOf(s) == textSource.getAsString().indexOf(s))
                appLogger.info(s+"\n");
        }
    }
}
