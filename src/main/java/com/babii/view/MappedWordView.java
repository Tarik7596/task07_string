package com.babii.view;

import java.util.function.Function;

public class MappedWordView extends AbsStreamView {
    private Function<String, String> mapper;

    public MappedWordView() {
        mapper = s -> s;
    }
    public MappedWordView(Function<String, String> m) {
        mapper = m;
    }
    void show() {
        textSource
                .wordsStream()
                .get()
                .filter(w -> w.length() > 2)
                .distinct()
                .map(mapper)
                .map(w -> w + "\n")
                .forEach(appLogger::info);
    }
}
