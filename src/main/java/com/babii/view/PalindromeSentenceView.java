package com.babii.view;

import com.babii.servicepack.TextProcessor;

public class PalindromeSentenceView extends AbsStreamView {
    void show(){
        textSource
                .sentenceStream()
                .get()
                .filter(s -> s.length() >0)
                .filter(TextProcessor::findPalindrome)
                .forEach(appLogger::info);
    }
}
