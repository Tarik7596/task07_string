package com.babii.view;

public class SentenceCutView extends AbsStreamView {
    private String cutSequence;
    private String replacement;

    public SentenceCutView(String cs) {
        cutSequence = cs + ".+" + cs;
    }
    public SentenceCutView(int length, String rpl) {
        cutSequence = "\\b[^eyuioa]\\w{" + (length - 1) + "}\\b";
        replacement = rpl;
    }
    void show(){
        textSource
                .sentenceStream()
                .get()
                .filter(s -> s.length() > 0)
                .map(s -> cutMarkedSequence(s) + "\n")
                .forEach(appLogger::info);
    }
    private String cutMarkedSequence(String sentence) {
        sentence = sentence.replaceFirst(cutSequence, replacement);
        return sentence;
    }
}
