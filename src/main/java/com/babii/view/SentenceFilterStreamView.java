package com.babii.view;

import java.util.Comparator;

public class SentenceFilterStreamView extends AbsStreamView {
    private Comparator<String> sortSentence;
    public SentenceFilterStreamView() {
        sortSentence = Comparator.naturalOrder();
    }
    public SentenceFilterStreamView(Comparator<String> sorterer) {
        sortSentence = sorterer;
    }
    void show() {
        textSource
                .sentenceStream()
                .get()
                .sorted(sortSentence)
                .map(s -> s + "\n")
                .forEach(appLogger::info);
    }
}
