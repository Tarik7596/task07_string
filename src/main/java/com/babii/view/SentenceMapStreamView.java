package com.babii.view;

import java.util.function.Function;

public class SentenceMapStreamView extends AbsStreamView {
    private Function<String, String> mapper;

    public SentenceMapStreamView(Function<String, String> m) {
        mapper = m;
    }

    void show() {
        textSource
                .sentenceStream()
                .get()
                .map(mapper)
                .forEach(appLogger::info);
    }
}
