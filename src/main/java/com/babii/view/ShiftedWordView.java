package com.babii.view;

public class ShiftedWordView extends AbsStreamView {
    void show() {
        String shift = "";
        String[] words =
                textSource
                        .wordsStream()
                        .get()
                        .distinct()
                        .filter(w -> w.length() > 1)
                        .sorted()
                        .toArray(String[]::new);
        for(int i = 0; i+1 < words.length; i++) {
            appLogger.info(shift + words[i] + "\n");
            if(words[i].charAt(0) - words[i+1].charAt(0) < 0) {
                shift += "\t";
            }
        }
    }
}
