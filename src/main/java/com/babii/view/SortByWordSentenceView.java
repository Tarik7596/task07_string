package com.babii.view;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SortByWordSentenceView extends AbsStreamView {
    private String wordTag;

    public SortByWordSentenceView(String word) {
        wordTag = "\\b" + word + "\\b";
    }
    void show() {
        textSource
                .sentenceStream()
                .get()
                .filter(s -> s.length() > 0)
                .sorted(Comparator.comparing(this::countWordOccurrence).reversed())
                .map(s -> s + "\n")
                .forEach(appLogger::info);
    }
    private int countWordOccurrence(String sentence) {
        int count = 0;
        sentence = sentence.trim();
        Pattern word = Pattern.compile(wordTag);
        Matcher target = word.matcher(sentence);
        while(target.find()) {
            count++;
        }
        return count;
    }
}
