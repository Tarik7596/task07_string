package com.babii.view;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SortedByLetterWordView extends AbsStreamView {
    private String letterTag;

    public SortedByLetterWordView(String letter) {
        letterTag = letter;
    }
    void show(){
        textSource
                .wordsStream()
                .get()
                .filter(w -> w.length() > 2)
                .distinct()
                .sorted(Comparator.comparingInt(this::countLetterOccurrence))
                .map(w -> w + "\n")
                .forEach(appLogger::info);

    }
    private int countLetterOccurrence(String word) {
        int count = 0;
        Pattern ltr = Pattern.compile(letterTag);
        Matcher target = ltr.matcher(word);
        while(target.find()) {
            count++;
        }
        return count;
    }
}
