package com.babii.view;

import java.util.Comparator;

public class SortedVowelWordView extends AbsStreamView {
    private Comparator<String> sortComparator;

    public SortedVowelWordView(Comparator<String> sc) {
        sortComparator = sc;
    }
    public SortedVowelWordView() {
        sortComparator = Comparator.naturalOrder();
    }
    void show() {
        textSource
                .wordsStream()
                .get()
                .distinct()
                .filter(w -> w.length() > 2)
                .filter(w -> w.matches("[eyuioa]\\w+\\b"))
                .sorted(sortComparator)
                .map(w -> w + "\n")
                .forEach(appLogger::info);
    }
}
