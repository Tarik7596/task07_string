package com.babii.view;

import java.util.Comparator;

public class SortedWordView extends AbsStreamView {
    private Comparator<String> sortComparator;

    public SortedWordView() {
        sortComparator = Comparator.naturalOrder();
    }
    public SortedWordView(Comparator<String> sc) {
        sortComparator = sc;
    }
    void show() {
        textSource
                .wordsStream()
                .get()
                .filter(w -> w.length() > 0)
                .distinct()
                .sorted(sortComparator)
                .map(w -> w + "\n")
                .forEach(appLogger::info);
    }
}
