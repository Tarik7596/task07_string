package com.babii.view;

import com.babii.model.Publishable;

public interface Updatable {
    void update(Publishable source);
}
