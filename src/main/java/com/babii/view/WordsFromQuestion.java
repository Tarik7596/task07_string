package com.babii.view;

import java.util.Arrays;

public class WordsFromQuestion extends AbsStreamView {
    private int wordLength;
    public WordsFromQuestion(int wl) {
        wordLength = wl;
    }
    void show() {
        textSource
                .sentenceStream()
                .get()
                .filter(s -> s.contains("?"))//length == from flatmap
                .flatMap(s -> Arrays.stream(s.split("\\W")))
                .filter(w -> w.length() == wordLength)
                .distinct()
                .map(s -> s + "\n")
                .forEach(appLogger::info);
    }
}
